clear
echo "======================================================="
echo "                      BIENVENIDO                       "
echo "         VirtualHost con Apache2 para Laravel          "
echo "======================================================="

echo "👀 Nota: El directorio de la aplicacion debe de encontrase en"
echo "         \"/var/www/\" y el nombre del directorio sin espacios"
echo "         en blanco                                  "
echo

echo "*******************************************************"
echo "🧰 Directorio de aplicaciones: \"/var/www/\""
ls /var/www/
echo "*******************************************************"
echo;


#obtenermos el directorio actual
path="$(pwd)";

#directorio de la aplicacion
read -p "📁 Ingrese directorio de la aplicacion: [/var/www/]" directorio 

#concatenamos el directorio y public (Laravel)
directorio_public="/var/www/""$(echo $directorio | tr -d '[[:space:]]')""/public"

#verificamos si existe el directorio
if test -d $directorio_public 
then
    echo "✅ Es un directorio valido."

    #nombre de usuario para configurar los permisos de la carpeta
    name_user_no_root="$(id -u -n)"

    #ejecutamos el segundo script
    echo;echo "🔐 Se requiere de permisos de usuario root"

    #ejecutable
    chmod a+x laravel-apache2.sh

    #$name_user_no_root  es $1 ---- $directorio es $2
    sudo ./laravel-apache2.sh $name_user_no_root $directorio

    echo;

    #verificamos que el proyecto tenga instalado composer 
    echo "⚙ Verificando el proyecto laravel..."

    #ejecutable
    chmod a+x laravel-configuracion.sh

    ./laravel-configuracion.sh $directorio

    read -p "❓ Desea crear la base de datos \"mysql\"? [S/n]" opcion

    if  [ "$opcion" = "S" ] || [ "$opcion" = "s" ]
    then 
        #ejecutable
        chmod a+x laravel-mysql.sh

        #ejecutamos el script de la creacion
        sudo ./laravel-mysql.sh $directorio
    else
        echo;
    fi

    echo;

    #limpiamos cache
    cd "/var/www/""$(echo $directorio | tr -d '[[:space:]]')"
    echo "✅ Eliminando archivos de cache"
    php artisan config:cache
    echo;echo "🚀 Ingrese a su web: http://www.$directorio.com"
    echo "🚀 Ingrese a su web: http://$directorio.com"
    cd $path



else
    echo;echo "❌ El directorio no es valido o no existe."
fi