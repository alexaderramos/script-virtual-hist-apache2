#!/bin/bash
#verificamos si esta instalado composer 


directorio="$(echo $1 | tr -d '[[:space:]]')"


#variable para .env
env_data="
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:cj3MJkY8/TWHkp3tt/wqJn3yS1wjxFmufe6HEJ0YG40=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DATABASE_URL=''
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME=$""{APP_NAME}""

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY=$""{PUSHER_APP_KEY}""
MIX_PUSHER_APP_CLUSTER=$""{PUSHER_APP_CLUSTER}"

#concatenamos el directorio y public (Laravel)
directorio_vendor="/var/www/""$(echo $directorio | tr -d '[[:space:]]')""/vendor"

#verificamos si existe el directorio
if test -d $directorio_vendor 
then
    echo "✅ Composer está instalado"
else
    echo "✳ Instalando Composer"
    composer -d "/var/www/""$(echo $directorio | tr -d '[[:space:]]')" install
fi


#verificamos si existe el archivo .env
archivo_env="/var/www/$directorio/.env"
if test -f $archivo_env
then
    echo;
    echo "✅ Existe el archivo .env"

    #agregamos la key
    #random numbers
    a=$(( $RANDOM % 10 ))
    b=$(( $RANDOM % 100 ))
    c=$(( $RANDOM % 1000 ))
    sed -i '/APP_KEY/c APP_KEY=base64:cj3'${a}'JkY8/TWHk'${b}'tt/wqJn3yS1wjxF'${c}'e6HEJ0YG40=' "${archivo_env}"
    echo;
else
    echo "📝 El archivo .env no existe. Se creará el archivo .env"
    echo "${env_data}" >> $archivo_env
fi
