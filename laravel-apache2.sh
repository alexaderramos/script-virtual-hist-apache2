#!/bin/bash
echo;
#$name_user_no_root  es $1 ---- $directorio es $2

#concatenamos el directorio y public (Laravel)
directorio_public="/var/www/""$(echo $2 | tr -d '[[:space:]]')""/public"

#nombre de dominio
dominio="$(echo $2 | tr -d '[[:space:]]')"

#variable con la configuracion para el host
configuracion="
<VirtualHost *:80>
	ServerName www.$dominio.com
	ServerAlias $dominio.com
	ServerAdmin $USER@localhost
	DocumentRoot $directorio_public
	ErrorLog $""{APACHE_LOG_DIR}""/error.log
	CustomLog $""{APACHE_LOG_DIR}/access.log combined
</VirtualHost>

#Para instalar un certificado ssl quitar los # de lo siguiente
#<VirtualHost *:443>
#   ServerName www.$dominio.com
#   ServerAlias $dominio.com
#	DocumentRoot $directorio_public
#   SSLEngine on
#   SSLCertificateFile	/etc/ssl/certs/your-ssl.pem
#  	SSLCertificateKeyFile /etc/ssl/private/your-ssl.key
#</VirtualHost>"

if [ "$(whoami)" = 'root' ] #requiere permisos de superusuario
then
    
    #agregamos la extencion para crear el archivo
    archivo_configuracion="/etc/apache2/sites-available/$dominio.conf" 

    #verificamos si existe un enlace creado
    enlace_archivo_configuracion="/etc/apache2/sites-enabled/$dominio.conf"
    if test -f $enlace_archivo_configuracion 
    then
        echo "🔴 $enlace_archivo_configuracion ya existe y será eliminado."
        rm $enlace_archivo_configuracion
    fi
    
    #verificamos si ya hay un archivo de configuracion creado
    if test -f $archivo_configuracion 
    then
		 	echo "🔴 $archivo_configuracion ya existe y será eliminado"
            rm $archivo_configuracion
    fi

    #creamos el archivo y agregamos el texto
    echo;echo "📝 Se creara el archivo: "$archivo_configuracion
    echo "$configuracion" >> "$archivo_configuracion"

    #creamos el enlace simbolico
    echo "📝 Se creara el enlace simbolico al archivo: "$enlace_archivo_configuracion
    ln -s "$archivo_configuracion" "$enlace_archivo_configuracion"

    #agregamos el sitio a nuestro host
    echo;echo "🎯 Registrando Nombre del Host (www.$dominio.com): "
    echo "127.0.1.1         www.$dominio.com          $dominio.com          #laravel-project " >> "/etc/hosts"

    #agregamos los permisos al nuevo proyecto
    echo;echo "🔑 Configurando los permisos para el proyecto: $dominio"
    adduser $1 www-data
    chown -R www-data:www-data /var/www/$2
    chmod -R g+rwX /var/www/$2


    #recargamos apache2 
    service apache2 restart


    if [ $? == 0 ]; then
        notify-send "VirtualHost para Laravel" "✅ VirtualHost creado con éxito."
        
        echo;echo "📢 Servicio de Apache2 reiniciado"
        
        echo;echo "🚀 Ingrese a su web: http://www.$dominio.com"
        echo "🚀 Ingrese a su web: http://$dominio.com"
        
    else
        notify-send "VirtualHost para Laravel" "🚫 Ocurrió un error."
    fi

else 
    echo "🚫 No tiene los permisos suficientes para continuar"
fi