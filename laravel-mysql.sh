#!/bin/bash

directorio="$(echo $1 | tr -d '[[:space:]]')"

read -p "👤 Usuario root del servidor mysql: " user
read -s -p "🔐 Contraseña root para el servidor mysql: " password
echo;

# Creamos la nueva base de datos
mysql -u ${user} -p${password} -e "CREATE DATABASE IF NOT EXISTS \`laravel_$1\` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
echo;


if [ $? == 0 ]; then
    notify-send "VirtualHost para Laravel" "✅ La base de datos laravel_$1 se ha creado con éxito."
    echo "✅ La base de datos laravel_$1 se ha creado con éxito."


    #verificamos si existe el archivo .env
    archivo_env="/var/www/$directorio/.env"
    if test -f $archivo_env
    then
        echo;
        echo "✏️ Actualizando el archivo .env"
        sed -i '/DB_DATABASE/c DB_DATABASE=laravel_'${directorio}'' "${archivo_env}"
        sed -i '/DB_USERNAME/c DB_USERNAME='${user}'' "${archivo_env}"
        sed -i '/DB_PASSWORD/c DB_PASSWORD='${password}'' "${archivo_env}"

        echo;
    fi

    
else
    notify-send "VirtualHost para Laravel" "🚫 No se relealizo la creacion de la base de datos :("
    echo "🚫 No se relealizo la creacion de la base de datos :("
fi
