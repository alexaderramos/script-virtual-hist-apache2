
# SCRIPT PARA VIRTUALHOST EN APACHE2 Y PROYECTOS LARAVEL - LINUX 

_Este escrip permitira crear virtual host de manera mas rapida_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para empezar_

* [Apache2](http://httpd.apache.org/) - Apache HTTP Server

_Buscar y Editar  esta seccion del archivo /etc/apache2/apache2.conf_


```
<Directory /var/www/>
	Options Indexes FollowSymLinks
	AllowOverride All #Por defecto esta: None
	Require all granted
</Directory>

```
![texto cualquiera por si no carga la imagen](img/5.png)

_Activar *"mod_rewrite"*, abrir una terminal y escribir lo siguiente:_

```
$ sudo a2enmod rewrite

```

_Reiniciar apache_

```
sudo systemctl restart apache2

```



### Instalación 🔧

_Para poder ejecutar este scrip vaya atravez de la terminal al directorio donde se encuentra el archivo main.sh_

_Una ves encontrado, ejecute el archivo, escribiendo en la terminal_

```
$ sh main.sh
```

_Y acontinuacion sigue los pasos que te piden._


## Ejecución ⚙️

_Muestra de ejecucion del script al crear un proyecto de Laravel.
En este ejemplo el proyecto se llama "blog" y esta ubicado en /var/www/_

* [Laravel](https://laravel.com/) - The PHP Framework For Web Artisans

### Ejemplo 🔩

_Al ejecutar el archivo main.sh, muestra lo siguente :_

```
=======================================================
                      BIENVENIDO                       
         VirtualHost con Apache2 para Laravel          
=======================================================
👀 Nota: El directorio de la aplicacion debe de encontrase en
         "/var/www/" y el nombre del directorio sin espacios
         en blanco                                  

📁 Ingrese directorio de la aplicacion:
...

```

_Nos pedira ingresar el nombre del directorio en el cual se encuentra nuestro proyecto y será verificado._

```
📁 Ingrese directorio de la aplicacion:
blog
✅ Es un directorio valido.

🔐 Se requiere de permisos de usuario root
[sudo] contraseña para usuario: 

```

_Ingresar la constraseña de usuario root y presionar ENTER para proseguir.
En caso de existir los archivos de configuracion anterior se eliminaran y se crearán nuevamente._

```

🔴 /etc/apache2/sites-enabled/blog.conf ya existe y será eliminado.
🔴 /etc/apache2/sites-available/blog.conf ya existe y será eliminado

📝 Se creara el archivo: /etc/apache2/sites-available/blog.conf
📝 Se creara el enlace simbolico al archivo: /etc/apache2/sites-enabled/blog.conf

```

_Lo siguiente será:_
 + Registrar el proyecto en el archivo hosts
 + Agregar el usuario al grupo "www-data" y los permisos necesarios 

 ```

🎯 Registrando Nombre del Host (www.blog.com): 

🔑 Configurando los permisos para el proyecto: blog
El usuario `usuario' ya es un miembro de `www-data'.

📢 Servicio de Apache2 reiniciado!
 ```

_En el archivo hosts se registrará lo siguiente:_

```
127.0.1.1         www.blog.com          blog.com          #laravel-project 
```

_Finalmente, podremos acceder a nuestro proyecto desde cualquier navegador_

```
🚀 Ingrese a su web: http://www.blog.com
🚀 Ingrese a su web: http://blog.com

```
### Capturas 📷

![texto cualquiera por si no carga la imagen](img/carbon.png)

![texto cualquiera por si no carga la imagen](img/final.png)


## Autores ✒️

* **Alexander Ramos** - *Trabajo Inicial* - [alexanderramos](https://gitlab.com/alexaderramos/)


---